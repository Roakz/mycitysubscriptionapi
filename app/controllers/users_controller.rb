class UsersController < ApplicationController

    def index
      render json: {:users => "All the users"}
    end

    def show
      render json: {:users => "Showing user"}
    end

    def create
      render json: {:users => "Creating a user"}
    end

    def update
      render json: {:users => "Update a user"}
    end

    def destroy
      render json: {:users => "User desctrution!"}
    end
end
